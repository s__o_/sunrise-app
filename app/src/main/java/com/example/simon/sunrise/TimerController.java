package com.example.simon.sunrise;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.simon.sunrise.Tasks.SetSunriseTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Simon on 27.08.18.
 */

public class TimerController {


    private static TimerController instance;


    private Timer timer;
    private int sleepTime = 450; // in minutes
    private String baseUrl;
    private Boolean shutdown;
    private static Context context = null;

    public TimerController (SharedPreferences sharedPref){
        this.baseUrl = sharedPref.getString("ip_address", "");
        if (this.baseUrl == null){
            this.baseUrl = "http://168.142.0.1";
        }
        String pref_duration = sharedPref.getString("sleep_duration", "7:30");
        if (pref_duration == null){
            pref_duration = "7:30";
        }
        this.shutdown = sharedPref.getBoolean("shutdown_system", true);
        if (this.shutdown == null){
            this.shutdown = true;
        }
        String [] time = pref_duration.split(":");
        this.sleepTime = Integer.parseInt(time[0]) * 60 + Integer.parseInt(time[1]);
        this.timer = idealDuration();

//        Log.v("TimerController", pref_ip);
//        Log.v("TimerController", pref_duration);
//        Log.v("TimerController", pref_shutdown + "");
    }

    public static synchronized TimerController getInstance () {
        if (TimerController.instance == null) {
//            TimerController.instance = new TimerController ();
        }
        return TimerController.instance;
    }

    public Timer idealDuration() {
        Calendar currTime = Calendar.getInstance();
        int currHour = currTime.get(Calendar.HOUR_OF_DAY);
        int currMinute = currTime.get(Calendar.MINUTE);

        int hour = currHour + ((this.sleepTime - (this.sleepTime % 60)) / 60);
        int minute = currMinute + (this.sleepTime % 60);
//        Log.v("TimerController", hour + "|" + minute);
        if(minute > 60){
            int h = minute % 60;
            hour = hour + ((minute - h) / 60);
            minute = h;
        }

        hour = hour % 24;

        return new Timer(hour, minute);
    }

    public void newTimer(int hour, int minute){
        timer = new Timer(hour, minute);
    }

    public Timer getTimer() {
        return timer;
    }

    public void setTimer(Timer timer) {
        this.timer = timer;
    }

    public int getSleepTime() {
        return sleepTime;
    }

    public void setSleepTime(int sleepTime) {
        this.sleepTime = sleepTime;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public Boolean getShutdown() {
        return shutdown;
    }

    public void setShutdown(Boolean shutdown) {
        this.shutdown = shutdown;
    }

    public void send(Context context, final Window window,
                     final Boolean alarm, final Boolean shutdown) {
        this.context = context;
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = String.format("http://%s/sunrise", this.getBaseUrl());

        // Request a string response from the provided URL.
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // Display the first 500 characters of the response string.
                                Log.v("SetSunriseTask", response);
                                Context context = TimerController.context;
                                Log.v("SetSunriseTask", getTimeDif() + "");
                                int diff = getTimeDif();
                                CharSequence text = context.getResources().getString(R.string
                                                .timer_success_msg,
                                        diff / 60 , diff % 60);
                                int duration = Toast.LENGTH_LONG;

                                Toast toast = Toast.makeText(context, text, duration);
                                toast.show();
                                hideLoadingAnimation(window);
                                getAlarm(context, window);
                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                               hideLoadingAnimation(window);
                                Log.v("SetSunriseTask", "That didn't work!");
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams()
                    {
                        Map<String, String>  params = new HashMap<String, String>();
                        params.put("hour", getTimer().getHour() + "");
                        params.put("minute", getTimer().getMinute() + "");
                        params.put("sound", alarm.toString());
                        params.put("shutdown", shutdown.toString());

                        return params;
                    }
                };
                /*
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.v("SetSunriseTask", response);
                        Context context = TimerController.context;
                        Log.v("SetSunriseTask", getTimeDif() + "");
                        int diff = getTimeDif();
                        CharSequence text = context.getResources().getString(R.string
                                        .timer_success_msg,
                                diff / 60 , diff % 60);
                        int duration = Toast.LENGTH_LONG;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    },
                            new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.v("SetSunriseTask", "That didn't work!");
                        }
                    }
                    ) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", "Alif");
                params.put("domain", "http://itsalif.info");

                return params;
            }
        }
                );
    */

        postRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(postRequest);
        this.showLoadingAnimation(window);
        Log.v("TimerController", "submit " + alarm);
    }

    public void delete(Context context, final Window window) {
        this.context = context;
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = String.format("http://%s/sunrise", this.getBaseUrl());

        // Request a string response from the provided URL.
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.DELETE, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getInt("status") == 200){
                                int duration = Toast.LENGTH_LONG;
                                Context context = TimerController.context;
                                CharSequence text = context.getResources().getString(R.string
                                                .alarm_deleted);
                                Toast toast = Toast.makeText(TimerController.context, text,
                                        duration);
                                hideLoadingAnimation(window);
                                toast.show();
                            }
                        } catch (JSONException e) {
                            hideLoadingAnimation(window);
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideLoadingAnimation(window);
                        Log.v("SetSunriseTask", "That didn't work!");
                    }
                });
        queue.add(jsonObjectRequest);
        this.showLoadingAnimation(window);
    }

    public void getAlarm(final Context context, final Window window) {
        this.context = context;
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = String.format("http://%s/sunrise", this.getBaseUrl());

        // Request a string response from the provided URL.
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
        (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                ProgressBar pb = window.findViewById(R.id.loadingWheel);
                pb.setVisibility(View.INVISIBLE);
                try {
                    if(response.getInt("status") == 200){
                        JSONObject alarm = response.getJSONObject("alarm");

                        TextView lll = window.findViewById(R.id.currentTimerLabel);
                        lll.setVisibility(View.VISIBLE);

                        LinearLayout ll = window.findViewById(R.id.currentTimer);
                        TextView timer = window.findViewById(R.id.currentTimerTime);
                        ll.setVisibility(View.VISIBLE);

                        ImageView soundIcon = window.findViewById(R.id.currentTimerSound);
                        if(alarm.getBoolean("sound")){
                            soundIcon.setVisibility(View.VISIBLE);
                        }else{
                            soundIcon.setVisibility(View.GONE);
                        }

                        ImageView shutDownIcon = window.findViewById(R.id.currentTimerShutDown);
                        if(alarm.getBoolean("shutdown")){
                            shutDownIcon.setVisibility(View.VISIBLE);
                        }else{
                            shutDownIcon.setVisibility(View.GONE);
                        }

                        TextView errorMsg = window.findViewById(R.id.currentTimerNoConnection);
                        errorMsg.setVisibility(View.INVISIBLE);

                        String timerText = "";

                        if (alarm.getInt("hour") < 10){
                            timerText += "0" + alarm.getInt("hour") + ":";
                        }else {
                            timerText += alarm.getInt("hour") + ":";
                        }

                        if (alarm.getInt("hour") < 10){
                            timerText += "0" + alarm.getInt("hour");
                        }else {
                            timerText += alarm.getInt("minute");
                        }
                        timer.setText(timerText);


                        Log.v("SetSunriseTask", "Alarm hour: " + alarm.get("hour"));
                    } else{
                        TextView errorMsg = window.findViewById(R.id.currentTimerNoConnection);
                        errorMsg.setVisibility(View.INVISIBLE);

                        Log.v("SetSunriseTask", "Request status: " + response.getString
                                ("message"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ProgressBar pb = window.findViewById(R.id.loadingWheel);
                pb.setVisibility(View.INVISIBLE);
                TextView lll = window.findViewById(R.id.currentTimerLabel);
                lll.setVisibility(View.INVISIBLE);

                LinearLayout ll = window.findViewById(R.id.currentTimer);
                ll.setVisibility(View.INVISIBLE);

                TextView errorMsg = window.findViewById(R.id.currentTimerNoConnection);
                errorMsg.setVisibility(View.VISIBLE);
                Log.v("SetSunriseTask", "That didn't work!");
            }
        });
        queue.add(jsonObjectRequest);
        ProgressBar pb = window.findViewById(R.id.loadingWheel);
        pb.setVisibility(View.VISIBLE);
    }

    public int getTimeDif(){
        Calendar currTime = Calendar.getInstance();
        int currHour = currTime.get(Calendar.HOUR_OF_DAY);
        int currMinute = currTime.get(Calendar.MINUTE);

        int up = (this.getTimer().getHour() * 60) + this.getTimer().getMinute();
        int down = (currHour * 60) + currMinute;

        Log.v("TimerController", up + "|"+ down);
        if (up > down) {
            return up - down;
        }else {
            return up + (1440 - down);
        }
    }

    private void showLoadingAnimation(Window w){
        ImageView rocketImage = (ImageView) w.findViewById(R.id.rocket_image);
        rocketImage.setVisibility(View.VISIBLE);
        rocketImage.bringToFront();
        ((AnimationDrawable) rocketImage.getDrawable()).start();
    }

    private void  hideLoadingAnimation(Window w){
        ImageView rocketImage = w.findViewById(R.id.rocket_image);
        ((AnimationDrawable) rocketImage.getDrawable()).stop();
        rocketImage.setVisibility(View.INVISIBLE);
    }
}
