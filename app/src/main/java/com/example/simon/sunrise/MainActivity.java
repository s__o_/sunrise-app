package com.example.simon.sunrise;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.simon.sunrise.Tasks.SetSunriseTask;

public class MainActivity extends AppCompatActivity
    implements TimepickerDialog.TimeDialogListener{

    private TimerController tc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);
        TextView currentTimerLabel = findViewById(R.id.currentTimerLabel);
        currentTimerLabel.setVisibility(View.INVISIBLE);

        LinearLayout currentTimer = findViewById(R.id.currentTimer);
        currentTimer.setVisibility(View.INVISIBLE);
    }


    public void setTime(View view) {
        Bundle data = new Bundle();//create bundle instance
        data.putInt("hour", this.tc.getTimer().getHour());//put string to pass with a key value
        data.putInt("minute", this.tc.getTimer().getMinute());

        DialogFragment dialog = new TimepickerDialog();
        dialog.setArguments(data);
        dialog.show(this.getFragmentManager(), "");

        Log.v("MainActivity", "set time");
    }

    public void submit(View view) {
        Switch s = findViewById(R.id.soundSwitch);
        Button sb = findViewById(R.id.submitButton);
//        sb.setVisibility(View.INVISIBLE);
        this.tc.send(this, getWindow(), s.isChecked(), this.tc.getShutdown());
    }

    public void deleteTimer(View view) {
        TextView currLabel = findViewById(R.id.currentTimerLabel);
        currLabel.setVisibility(View.INVISIBLE);

        LinearLayout ll = findViewById(R.id.currentTimer);
        ll.setVisibility(View.INVISIBLE);
        tc.delete(this, getWindow());
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        TimePicker tp = dialog.getDialog().findViewById(R.id.timePicker);
        TextView timeLabel = findViewById(R.id.timeLabel);
        int hour = tp.getHour();
        int minute = tp.getMinute();

        this.tc.newTimer(hour, minute);

        timeLabel.setText(tc.getTimer().toString());
    }

    @Override
    public void onDialogNeutralClick(DialogFragment dialog) {
        this.tc.setTimer(this.tc.idealDuration());
        TextView timeLabel = findViewById(R.id.timeLabel);
        timeLabel.setText(tc.getTimer().toString());
        Log.v(toString(), "negative");
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        Log.v(toString(), "negative");
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        tc = new TimerController(sharedPref);
        LinearLayout currentTimer = findViewById(R.id.currentTimer);
        tc.getAlarm(this, getWindow());


        TextView timerLabel = findViewById(R.id.timeLabel);
        timerLabel.setText(this.tc.getTimer().toString());
    }

    public void openSettings(View view) {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }
}
