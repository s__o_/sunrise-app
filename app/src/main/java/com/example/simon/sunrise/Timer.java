package com.example.simon.sunrise;

/**
 * Created by Simon on 27.08.18.
 */

public class Timer {

    private int hour;
    private int minute;

    public Timer(int hour, int minute) {
        this.hour = hour;
        this.minute = minute;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public String toString(){
        String h = this.hour + "";
        String m = this.minute + "";

        if(this.hour < 10){
            h = "0" + this.hour;
        }

        if(this.minute < 10){
            m = "0" + this.minute;
        }

        return h + ":" + m;
    }
}
