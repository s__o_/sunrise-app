package com.example.simon.sunrise.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.simon.sunrise.MainActivity;
import com.example.simon.sunrise.Timer;
import com.example.simon.sunrise.TimerController;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Simon on 30.08.18.
 */

public class SetSunriseTask extends AsyncTask<Timer, Void, String> {

    @Override
    protected String doInBackground(Timer ... t) {
        Timer timer = t[0];
//            URL url = new URL("http://" + "192.168.1.4:5000" +
//                    "/setSunrise"+"?H="+timer.getHour()+"&M="+timer.getMinute()
//                    +"&off=false");


            // Instantiate the RequestQueue.
//            RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
//            String url ="http://192.168.1.4:5000/setSunrise?H=5&M=10&off=false";
//
//            // Request a string response from the provided URL.
//            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
//                    new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String response) {
//                            // Display the first 500 characters of the response string.
//                            Log.v("SetSunriseTask", response);
//                        }
//                    }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    Log.v("SetSunriseTask", "That didn't work!");
//                }
//            });
//
//            // Add the request to the RequestQueue.
//            queue.add(stringRequest);
        return "Failed";
    }

    protected void onPostExecute(String result) {
        Log.v("SetSunriseTask", result);
    }
}
