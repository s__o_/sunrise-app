# Sunrise App

![logo](/app/src/main/res/mipmap-xxxhdpi/ic_launcher_round.png)

This app provides a simple interface to set a new wake uptime for the custom "sunrise alarm clock". With that, it complements the setup for the custom "alarm clock", consisting of an RGB Yeelight, [Flask API backend](https://gitlab.com/s__o_/sunrise-server) (deployed on a Raspberry Pi) and this android application.

## Impressions

![main activity](/pictures/main_panel.png)

## Requirements
